package com.example.arundhati.studentlist.utils;

/**
 * Created by Arundhati on 9/8/2015.
 */
public interface MyDialogListener {
    void onClick(int position);
}
