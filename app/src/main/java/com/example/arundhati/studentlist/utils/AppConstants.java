package com.example.arundhati.studentlist.utils;

/**
 * Created by Arundhati on 9/8/2015.
 */
public interface AppConstants {

    int EDIT = 1;
    int DELETE = 2;
    String ROLL_NO = "Roll_No";
    String NAME = "Name";
    int EDIT_REQUEST_CODE = 100;
    String EDIT_TEXT = "Edit Information";
    String DISPLAY_TEXT = "Display Information";
    String DIALOG = "My Dialog";
    String TEXT = "Student List";
    String TOAST_TEXT = "Enter Data";
    String NUMBER_TOAST_TEXT = "Enter 10 digit mobile number";
    int NUM_OF_COLUMNS = 2;
    String LOGIN = "Login_data";
    String USERNAME = "admin";
    String PASSWORD = "qwerty";
    int CANCEL = 50;
    int ADD = 10;
    int VIEW_SPECIFIC = 20;
    int VIEW_ALL = 30;
    int UPDATE = 40;
    int ADD_STUDENT = 0;
    int LOGOUT = 1;
    String ADD_DATA_FRAGMENT_TAG = "add_Data_Fragment";
    String ADD_STUDENT_TITLE = "Add Student";
    String LOGOUT_TITLE = "Logout";

}
