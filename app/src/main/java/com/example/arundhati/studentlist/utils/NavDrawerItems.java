package com.example.arundhati.studentlist.utils;

/**
 * Created by Arundhati on 9/30/2015.
 */
public class NavDrawerItems {

    private String title;
    private int icon;

    public NavDrawerItems(String title, int icon) {
        this.title = title;
        this.icon = icon;
    }

    public String getTitle() {
        return this.title;
    }

    public int getIcon() {
        return this.icon;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }
}
