package com.example.arundhati.studentlist.activities;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import com.example.arundhati.studentlist.adapters.NavDrawerListAdapter;
import com.example.arundhati.studentlist.comparisonClasses.CompareByName;
import com.example.arundhati.studentlist.comparisonClasses.CompareByRollNo;
import com.example.arundhati.studentlist.entities.Student;
import com.example.arundhati.studentlist.dialogs.MyDialog;
import com.example.arundhati.studentlist.R;
import com.example.arundhati.studentlist.adapters.MyAdapter;
import com.example.arundhati.studentlist.utils.AppConstants;
import com.example.arundhati.studentlist.utils.DBController;
import com.example.arundhati.studentlist.utils.MyDialogListener;
import com.example.arundhati.studentlist.utils.NavDrawerItems;
import com.example.arundhati.studentlist.utils.OnItemClickListener;
import com.example.arundhati.studentlist.utils.ReceiveStudentData;
import com.example.arundhati.studentlist.utils.SendStudentData;

public class Details extends AppCompatActivity implements AppConstants, MyDialogListener, ReceiveStudentData, OnItemClickListener, View.OnClickListener, AdapterView.OnItemSelectedListener, AdapterView.OnItemClickListener {

    int positionId;
    Spinner spinner;
    public static int studentRollNo;
    Button listButton, gridButton;
    MyAdapter adapter;
    TextView textView;
    MyDialog dialog;
    RecyclerView recyclerView;
    LinkedList<Student> studentData;
    SharedPreferences preferences;
    RelativeLayout detailsLayout;
    ArrayAdapter<String> adapterSpinner;
    AddData addDataFragment;
    FragmentManager manager;
    FragmentTransaction transaction;
    Student studentObj;
    SendStudentData sendStudentData;
    ListView drawerList;
    DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    private ArrayList<NavDrawerItems> navDrawerItems;
    private NavDrawerListAdapter navDrawerListAdapter;
    private CharSequence mTitle, mDrawerTitle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        initialization();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.app_name, R.string.app_name) {
            public void onDrawerClosed(View view) {
                getSupportActionBar().setTitle(mTitle);
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                getSupportActionBar().setTitle(mDrawerTitle);
                invalidateOptionsMenu();
            }
        };
        drawerToggle.setDrawerIndicatorEnabled(true);
        drawerLayout.setDrawerListener(drawerToggle);
        setListeners();

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle action bar actions click
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getSupportActionBar().setTitle(mTitle);
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        drawerToggle.onConfigurationChanged(newConfig);
    }


    //function to initialize all the components
    public void initialization() {
        preferences = getSharedPreferences("Login_data", Context.MODE_PRIVATE);
        studentData = new LinkedList<>();
        studentObj = new Student();
        detailsLayout = (RelativeLayout) findViewById(R.id.detailLayout);
        listButton = (Button) findViewById(R.id.listButton);
        gridButton = (Button) findViewById(R.id.gridButton);
        textView = (TextView) findViewById(R.id.textView);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        spinner = (Spinner) findViewById(R.id.spinner);
        adapter = new MyAdapter(studentData, this, R.layout.row_layout);
        recyclerView.setAdapter(adapter);
        new BackgroundTask().execute(VIEW_ALL);
        adapter.notifyDataSetChanged();
        adapterSpinner = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.spinnerList));
        adapterSpinner.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapterSpinner);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        drawerList = (ListView) findViewById(R.id.drawerList);
        mTitle = mDrawerTitle = getTitle();
        navDrawerItems = new ArrayList<NavDrawerItems>();
        navDrawerItems.add(new NavDrawerItems(ADD_STUDENT_TITLE, R.drawable.add_user));
        navDrawerItems.add(new NavDrawerItems(LOGOUT_TITLE, R.drawable.logout_image));
        navDrawerListAdapter = new NavDrawerListAdapter(getApplicationContext(), R.layout.drawer_list_layout, navDrawerItems);
        drawerList.setAdapter(navDrawerListAdapter);

    }

    //function to set listeners on the components
    public void setListeners() {
        listButton.setOnClickListener(this);
        gridButton.setOnClickListener(this);
        adapter.setOnItemClickListener(this);
        spinner.setOnItemSelectedListener(this);
        drawerList.setOnItemClickListener(this);


    }

    @Override
    public void onClick(int position) {
        switch (position) {
            case EDIT:
                addDataFragment = new AddData();
                manager = getFragmentManager();
                transaction = manager.beginTransaction();
                transaction.add(R.id.studentDataLayout, addDataFragment, ADD_DATA_FRAGMENT_TAG);
                transaction.commit();
                new BackgroundTask().execute(EDIT);
                break;
            case VIEW_SPECIFIC:
                addDataFragment = new AddData();
                manager = getFragmentManager();
                transaction = manager.beginTransaction();
                transaction.add(R.id.studentDataLayout, addDataFragment, ADD_DATA_FRAGMENT_TAG);
                transaction.commit();
                new BackgroundTask().execute(VIEW_SPECIFIC);
                break;
            case DELETE:
                new BackgroundTask().execute(DELETE);
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.listButton:

                recyclerView.setLayoutManager(new LinearLayoutManager(this));
                break;

            case R.id.gridButton:

                recyclerView.setLayoutManager(new StaggeredGridLayoutManager(NUM_OF_COLUMNS, StaggeredGridLayoutManager.VERTICAL));
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        String sortBy = spinner.getSelectedItem().toString();
        if (sortBy.equals(ROLL_NO)) {
            Collections.sort(studentData, new CompareByRollNo());
            adapter.notifyDataSetChanged();
        } else if (sortBy.equals(NAME)) {
            Collections.sort(studentData, new CompareByName());
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    @Override
    public void onItemClick(int position) {

        dialog = new MyDialog();
        dialog.setDialogListener(Details.this);
        positionId = position;
        dialog.show(getFragmentManager(), DIALOG);

    }

    @Override
    public void getData(Student student, int choice) {

        getFragmentManager().beginTransaction().remove(addDataFragment).commit();
        detailsLayout.setVisibility(View.VISIBLE);
        studentObj = student;
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        switch (choice) {

            case ADD:

                textView.setText(TEXT);

                new BackgroundTask().execute(ADD);
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                adapterSpinner.notifyDataSetChanged();
                break;

            case EDIT_REQUEST_CODE:

                new BackgroundTask().execute(UPDATE);
                break;

            case CANCEL:
                break;

        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (position) {
            case ADD_STUDENT:
                detailsLayout.setVisibility(View.GONE);
                addDataFragment = new AddData();
                manager = getFragmentManager();
                transaction = manager.beginTransaction();
                transaction.add(R.id.studentDataLayout, addDataFragment,ADD_DATA_FRAGMENT_TAG);
                transaction.commit();
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                drawerLayout.closeDrawer(drawerList);
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                getSupportActionBar().setHomeButtonEnabled(false);

                break;
            case LOGOUT:
                preferences.edit().clear().commit();
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                finish();
                break;

        }
    }

    class BackgroundTask extends AsyncTask<Integer, String, Integer> {

        RecyclerView.Adapter myAdapter;
        DBController controller;
        Student temp;
        int result = -2;
        int choice;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            controller = new DBController(Details.this);
            myAdapter = recyclerView.getAdapter();
            temp = new Student();
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected Integer doInBackground(Integer... params) {
            choice = params[0];
            switch (choice) {

                case VIEW_SPECIFIC:

                    temp = controller.viewSpecific(studentRollNo);
                    break;

                case VIEW_ALL:

                    studentData = controller.viewAll(studentData);
                    break;

                case EDIT:

                    temp = controller.viewSpecific(studentRollNo);
                    break;

                case DELETE:

                    result = (int) controller.deleteData(studentData.get(positionId));
                    studentData.remove(positionId);
                    break;

                case UPDATE:

                    result = (int) controller.updateData(studentObj);
                    break;

                case ADD:

                    result = (int) controller.insetData(studentObj);
                    studentObj.setRollNo(result);
                    break;
            }
            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            if (manager != null)
                sendStudentData = (SendStudentData) manager.findFragmentByTag(ADD_DATA_FRAGMENT_TAG);
            switch (choice) {
                case EDIT:
                    sendStudentData.sendData(temp, EDIT_REQUEST_CODE);
                    break;
                case VIEW_SPECIFIC:
                    sendStudentData.sendData(temp, VIEW_SPECIFIC);
                    break;
            }

            if (result > 0) {
                new BackgroundTask().execute(VIEW_ALL);
            }
            if (spinner.getSelectedItem().toString().equals(NAME)) {
                Collections.sort(studentData, new CompareByName());
            }
            myAdapter.notifyDataSetChanged();

        }
    }
}



