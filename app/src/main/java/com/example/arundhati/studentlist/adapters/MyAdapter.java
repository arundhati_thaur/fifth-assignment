package com.example.arundhati.studentlist.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.arundhati.studentlist.R;
import com.example.arundhati.studentlist.activities.Details;
import com.example.arundhati.studentlist.entities.Student;
import com.example.arundhati.studentlist.utils.OnItemClickListener;

import java.util.List;

/**
 * Created by Arundhati on 9/4/2015.
 */
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.Holder> {
    List<Student> data;
    Context context;
    int rowLayout;
    OnItemClickListener onItemClickListener;

    public MyAdapter(List<Student> data, Context context, int rowLayout) {
        this.data = data;
        this.context = context;
        this.rowLayout = rowLayout;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(rowLayout, null);
        Holder viewHolder = new Holder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(Holder holder, final int position) {

        holder.studentRollNo.setText(String.valueOf(data.get(position).getRollNo()));
        holder.studentName.setText(data.get(position).getName());
        holder.studentContact.setText(String.valueOf(data.get(position).getMobileNumber()));
        holder.address.setText(data.get(position).getAddress());
        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Details.studentRollNo = data.get(position).getRollNo();
                onItemClickListener.onItemClick(position);
            }
        });

    }


    @Override
    public long getItemId(int position) {
        return data.get(position).getRollNo();
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }


    public class Holder extends RecyclerView.ViewHolder {
        TextView studentRollNo, studentName, studentContact, address;
        LinearLayout container;

        public Holder(View view) {
            super(view);
            container = (LinearLayout) view.findViewById(R.id.container);
            studentRollNo = (TextView) view.findViewById(R.id.studentRollNo);
            studentName = (TextView) view.findViewById(R.id.name);
            studentContact = (TextView) view.findViewById(R.id.mobile);
            address = (TextView) view.findViewById(R.id.address);
        }
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}
