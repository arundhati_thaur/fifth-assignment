package com.example.arundhati.studentlist.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Arundhati on 9/16/2015.
 */
public class DBHelper extends SQLiteOpenHelper implements DBConstants {

    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, null, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTable = "CREATE TABLE " + TABLE_NAME + "(" + ROLL_NO + " INTEGER PRIMARY KEY , " + NAME + " TEXT , "
                + MOBILE_NO + " NUMBER , " + ADDRESS + " TEXT );";

        db.execSQL(createTable);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        String dropTable = "DROP TABLE IF EXISTS " + TABLE_NAME;
        db.execSQL(dropTable);
    }
}
