package com.example.arundhati.studentlist.utils;

/**
 * Created by Arundhati on 9/16/2015.
 */
public interface DBConstants {
    String DATABASE_NAME = "Student_Details";
    String TABLE_NAME = "student";
    String ROLL_NO = "_rollNo";
    String NAME = "name";
    String MOBILE_NO = "mobile_no";
    String ADDRESS = "address";

}
