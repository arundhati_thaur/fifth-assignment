package com.example.arundhati.studentlist.utils;

import com.example.arundhati.studentlist.entities.Student;

/**
 * Created by Arundhati on 9/29/2015.
 */
public interface ReceiveStudentData {
    void getData(Student student,int choice);
}
