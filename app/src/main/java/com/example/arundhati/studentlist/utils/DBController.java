package com.example.arundhati.studentlist.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.arundhati.studentlist.entities.Student;

import java.util.LinkedList;

/**
 * Created by Arundhati on 9/16/2015.
 */
public class DBController implements DBConstants {

    SQLiteDatabase db;
    DBHelper dbHelper;

    public DBController(Context context) {
        dbHelper = new DBHelper(context, DATABASE_NAME, null, 7);
    }
public  long newId()
{int id=0;
    db = dbHelper.getWritableDatabase();
    String viewAll = "SELECT * FROM " + TABLE_NAME + ";";
    Cursor cursor = db.rawQuery(viewAll, null);
    if (cursor.moveToFirst()) {
   cursor.moveToLast();
        id=cursor.getInt(0);
    }
    db.close();
    id++;
    return id;
}
    public long insetData(Student student) {
        db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(NAME, student.getName());
        values.put(MOBILE_NO, student.getMobileNumber());
        values.put(ADDRESS, student.getAddress());
        long id = db.insert(TABLE_NAME, null, values);
        db.close();
        return id;
    }

    public long updateData(Student student) {
        db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(NAME, student.getName());
        values.put(MOBILE_NO, student.getMobileNumber());
        values.put(ADDRESS, student.getAddress());
        long id = db.update(TABLE_NAME, values, ROLL_NO + "=?", new String[]{String.valueOf(student.getRollNo())});
        db.close();
        return id;
    }

    public long deleteData(Student student) {
        db =dbHelper.getWritableDatabase();
        long id = db.delete(TABLE_NAME, ROLL_NO + "=?", new String[]{String.valueOf(student.getRollNo())});
        db.close();
        return id;

    }

    public Student viewSpecific(int studentId) {
        db = dbHelper.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, null, ROLL_NO + "=?", new String[]{String.valueOf(studentId)}, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();

        Student student = new Student();
        student.setRollNo(cursor.getInt(0));
        student.setName(cursor.getString(1));
        student.setMobileNumber(Long.parseLong(cursor.getString(2)));
        student.setAddress(cursor.getString(3));
        db.close();
        return student;
    }

    public LinkedList<Student> viewAll(LinkedList<Student> list) {
        list.clear();
        db = dbHelper.getReadableDatabase();
        String viewAll = "SELECT * FROM " + TABLE_NAME + ";";
        Cursor cursor = db.rawQuery(viewAll, null);
        Student student;
        if (cursor.moveToFirst()) {
            do {
                student = new Student();
                student.setRollNo(cursor.getInt(0));
                student.setName(cursor.getString(1));
                student.setMobileNumber(Long.parseLong(cursor.getString(2)));
                student.setAddress(cursor.getString(3));
                list.add(student);

            } while (cursor.moveToNext());
        }
        db.close();
        return list;
    }
}
