package com.example.arundhati.studentlist.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.arundhati.studentlist.R;
import com.example.arundhati.studentlist.utils.AppConstants;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, AppConstants {
    EditText userName, password;
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button login = (Button) findViewById(R.id.login);
        Button exit = (Button) findViewById(R.id.exit);
        userName = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        preferences = getSharedPreferences(LOGIN, Context.MODE_PRIVATE);
        login.setOnClickListener(this);
        exit.setOnClickListener(this);

        if ((USERNAME.equals(preferences.getString("name", ""))) && (PASSWORD.equals(preferences.getString("password", "")))) {
            Intent intentObj = new Intent(this, Details.class);
            startActivity(intentObj);
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        String loginUserName = userName.getText().toString();
        String loginPassword = password.getText().toString();
        switch (v.getId()) {
            case R.id.login:
                if (loginUserName.equals(USERNAME) && loginPassword.equals(PASSWORD)) {
                    Intent intentObj = new Intent(this, Details.class);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("name", USERNAME);
                    editor.putString("password", PASSWORD);
                    editor.commit();
                    startActivity(intentObj);
                    finish();
                } else
                    Toast.makeText(this, "Enter Correct Login Details", Toast.LENGTH_SHORT).show();
                break;
            case R.id.exit:
                ActivityCompat.finishAffinity(this);
                break;
        }
    }
}
