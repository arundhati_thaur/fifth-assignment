package com.example.arundhati.studentlist.utils;

/**
 * Created by Arundhati on 9/14/2015.
 */
public interface OnItemClickListener {
    void onItemClick(int position);
}
