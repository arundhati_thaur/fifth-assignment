package com.example.arundhati.studentlist.adapters;


import android.content.Context;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.arundhati.studentlist.R;
import com.example.arundhati.studentlist.utils.NavDrawerItems;
import java.util.ArrayList;

/**
 * Created by Arundhati on 9/30/2015.
 */
public class NavDrawerListAdapter extends ArrayAdapter<NavDrawerItems> {

    private Context context;
    private ArrayList<NavDrawerItems> navDrawerItems;

    public NavDrawerListAdapter(Context context,int layoutResourseId, ArrayList<NavDrawerItems> navDrawerItems){
        super(context,layoutResourseId,navDrawerItems);
        this.context = context;
        this.navDrawerItems = navDrawerItems;
    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = View.inflate(context,R.layout.drawer_list_layout,null);
        ImageView imgIcon = (ImageView) convertView.findViewById(R.id.icon);
        TextView textView = (TextView) convertView.findViewById(R.id.title);
        imgIcon.setImageResource(navDrawerItems.get(position).getIcon());
        textView.setText(navDrawerItems.get(position).getTitle());
         return convertView;
    }

}