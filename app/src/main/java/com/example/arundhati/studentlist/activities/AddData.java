package com.example.arundhati.studentlist.activities;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.arundhati.studentlist.R;
import com.example.arundhati.studentlist.entities.Student;
import com.example.arundhati.studentlist.utils.AppConstants;
import com.example.arundhati.studentlist.utils.ReceiveStudentData;
import com.example.arundhati.studentlist.utils.SendStudentData;

/**
 * Created by Arundhati on 9/29/2015.
 */
public class AddData extends Fragment implements AppConstants, View.OnClickListener, SendStudentData {
    EditText studentName;
    EditText studentContact;
    EditText studentAddress;
    int studentRollNo;
    Student student;
    Button saveButton;
    ReceiveStudentData receiveStudentData;
    boolean isEditable;
    TextView textView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.add_data_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        textView = (TextView) getActivity().findViewById(R.id.textView);
        saveButton = (Button) getActivity().findViewById(R.id.saveButton);
        final Button button2 = (Button) getActivity().findViewById(R.id.cancelButton);
        studentName = (EditText) getActivity().findViewById(R.id.editText1);
        studentContact = (EditText) getActivity().findViewById(R.id.editText2);
        studentAddress = (EditText) getActivity().findViewById(R.id.editText3);
        saveButton.setOnClickListener(this);
        button2.setOnClickListener(this);
        receiveStudentData = (ReceiveStudentData) getActivity();

/*        if (getActivity().getIntent().hasExtra(STUDENT_KEY)) {
            student = (Student) getActivity().getIntent().getSerializableExtra(STUDENT_KEY);
            isEditable = getActivity().getIntent().getBooleanExtra(IS_EDITABLE_KEY, true);
        }
        if (student != null) {
            studentName.setText(student.getName());
            studentContact.setText(String.valueOf(student.getMobileNumber()));
            studentAddress.setText(student.getAddress());
            studentName.setEnabled(isEditable);
            studentContact.setEnabled(isEditable);
            studentAddress.setEnabled(isEditable);
            if (!isEditable) {
                textView.setText(DISPLAY_TEXT);
                saveButton.setVisibility(View.INVISIBLE);
                studentName.setTextColor(Color.rgb(0, 0, 0));
                studentContact.setTextColor(Color.rgb(0, 0, 0));
                studentAddress.setTextColor(Color.rgb(0, 0, 0));

            } else {
                textView.setText(EDIT_TEXT);
                saveButton.setVisibility(View.VISIBLE);
            }
        }*/

       /* try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }


    @Override
    public void onClick(View v) {
        student = new Student();
        String name = studentName.getText().toString();
        String mobileNumber = studentContact.getText().toString();
        String address = studentAddress.getText().toString();
        switch (v.getId()) {
            case R.id.saveButton:

                if (!name.isEmpty() && (!mobileNumber.isEmpty()) && (mobileNumber.length() == 10) && !address.isEmpty()) {

                    if (isEditable) {
                        student.setRollNo(studentRollNo);
                        student.setName(name);
                        student.setAddress(address);
                        student.setMobileNumber(Long.parseLong(mobileNumber));
                        receiveStudentData.getData(student, EDIT_REQUEST_CODE);
                    } else {
                        student.setName(name);
                        student.setAddress(address);
                        student.setMobileNumber(Long.parseLong(mobileNumber));
                        receiveStudentData.getData(student, ADD);
                    }
                } else if (name.isEmpty() || mobileNumber.isEmpty() || address.isEmpty()) {
                    Toast.makeText(getActivity(), TOAST_TEXT, Toast.LENGTH_SHORT).show();
                } else if (mobileNumber.length() < 10)
                    Toast.makeText(getActivity(), NUMBER_TOAST_TEXT, Toast.LENGTH_SHORT).show();
                break;

            case R.id.cancelButton:

                receiveStudentData.getData(null, CANCEL);
                break;
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            getActivity().onBackPressed();
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public void sendData(Student obj, int choice) {
        student = obj;
        studentRollNo = student.getRollNo();
        studentName.setText(student.getName());
        studentContact.setText(String.valueOf(student.getMobileNumber()));
        studentAddress.setText(student.getAddress());
        switch (choice) {
            case EDIT_REQUEST_CODE:
                isEditable = true;
                studentName.setEnabled(isEditable);
                studentContact.setEnabled(isEditable);
                studentAddress.setEnabled(isEditable);
                textView.setText(EDIT_TEXT);
                saveButton.setVisibility(View.VISIBLE);
                break;
            case VIEW_SPECIFIC:
                isEditable = false;
                studentName.setEnabled(isEditable);
                studentContact.setEnabled(isEditable);
                studentAddress.setEnabled(isEditable);
                textView.setText(DISPLAY_TEXT);
                saveButton.setVisibility(View.INVISIBLE);
                studentName.setTextColor(Color.rgb(0, 0, 0));
                studentContact.setTextColor(Color.rgb(0, 0, 0));
                studentAddress.setTextColor(Color.rgb(0, 0, 0));
                break;
        }

    }
}
